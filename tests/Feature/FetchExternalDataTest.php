<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FetchExternalData extends TestCase
{
    /**
     * Test custom token authentication.
     */
    public function testTokenAuth()
    {
        $response = $this->get('/api/ext/fetch', [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . env('API_SECRET'),
            'Content-Type' => 'application/json',
            'X-Test-Auth' => '1'
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test fetching data from third party APIs.
     */
    public function testDataRequest() {
        $response = $this->get('/api/ext/fetch', [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . env('API_SECRET'),
            'Content-Type' => 'application/json'
        ]);

        $response->assertStatus(200);
        $response->assertJsonPath('status', 'success');
    }
}
