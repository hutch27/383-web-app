<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class PlatformTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCurlExtensionAvailable()
    {
        $this->assertTrue(extension_loaded('curl'));
    }
}
