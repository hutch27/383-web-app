import React from 'react';
import Rating from './Rating';



class Card extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { category, city, description, name, rating, uri, thumbnail } = this.props.location;

		return (
			<div className="card">
				<a href={ uri }>
					<div className="content">
						<div className="main">
							<img src={ thumbnail } alt={ name } />

							<h3 className="font-bold text-2xl text-center">{ name }</h3>

							<div className="meta flex justify-between">
								<p className="capitalize flex">{ city }</p>
								<p className="flex text-right">{ category }</p>
							</div>
						</div>

						<div className="details">
							<p>{ description }</p>
							<Rating value={rating} />
						</div>
					</div>
				</a>
			</div>
		);
	}
}

export default Card;
