import React from 'react';
import Card from './Card';
import Filter from './Filter';



class LocationFeed extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			categories: [],
			cities: [],
			locations: [],
			token: null
		};
	}

	async componentDidMount() {
		// Get API token
		this.state.token = document.querySelector('input[name="token"]').value;

		// Fetch data
		this.updateCategories();
		this.updateCities();
		this.updateLocations();
	}

	render() {
		return (
			<div className="location-container">
				<Filter categories={this.state.categories} cities={this.state.cities} />

				<hr />

				{ this.state.locations.length > 0
					? <div className="locations flex flex-wrap justify-center">
						{ this.state.locations.map((loc) => {
							return (
								<Card key={ loc.id } location={ loc } />
								);
							}) }
					</div>

					: <div className="locations flex flex-wrap justify-center">
						<p>No results match your search.</p>
					</div>
				}
			</div>
		);
	}

	/**
	 * Perform http request via Fetch API.
	 */
	async request(url) {
		const response = await fetch(url, {
			headers: {
				'Accept': 'application/json',
				'Authorization': `Bearer ${this.state.token}`,
				'Content-Type': 'application/json'
			},
			method: 'GET'
		});
		let json = null;

		try {
			json = await response.clone().json();
		} catch (e) {
			console.error(e);
			console.log(await response.text());
		}

		if (Array.isArray(json.data)) {
			return json.data;
		}

		return [];
	}

	/**
	 * Update available filter categories.
	 */
	async updateCategories() {
		const categories = await this.request('/api/location/categories');

		this.setState({ categories });
	}

	/**
	 * Update available filter cities.
	 */
	async updateCities() {
		const cities = await this.request('/api/location/cities');

		this.setState({ cities });
	}

	/**
	 * Update shown locations.
	 *
	 * @param {array} params
	 */
	async updateLocations(params = {}) {
		const encodedParams = [];

		if (!params.page) {
			params.page = 0;
		}

		for (const [key, value] of Object.entries(params)) {
			if (value.length > 0 && value !== '0') {
				encodedParams.push(`${key}=${encodeURIComponent(value)}`);
			}
		}

		const locations = await this.request(`/api/location/filter/?${encodedParams.join('&')}`);

		this.setState({ locations });
	}
}

export default LocationFeed;
