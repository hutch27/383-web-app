import React from 'react';




class Filter extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="filter">
				<form className="location-filter">
					<label>
						Search
						<input name="query" type="search" />
					</label>

					<div className="spacer"></div>

					<span>Filter by:</span>

					<select name="category" defaultValue={0}>
						<option value="0">Category...</option>
						{
							this.props.categories.map((cat, index) => {
								return (
									<option key={ `${cat.replace(/[^a-z]/, '')}-${index}` } value={ cat }>{ cat }</option>
								);
							})
						}
					</select>

					<select name="city" defaultValue={0}>
						<option value="0">City...</option>
						{
							this.props.cities.map((cit, index) => {
								return (
									<option className="capitalize" key={ `${cit.replace(/[^a-z]/, '')}-${index}` } value={ cit }>{ cit }</option>
								);
							})
						}
					</select>

					<div className="spacer"></div>

					<button className="filter-submit" type="button">Submit</button>
				</form>
			</div>
		);
	}
}

export default Filter;
