import React from 'react';



class Rating extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const split = this.props.value.split('/');
		const numerator = split[0];
		const denominator = split[1];

		return (
			<div className="rating">
				{ numerator } out of { denominator } stars.
			</div>
		);
	}
}

export default Rating;
