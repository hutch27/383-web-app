import LocationFeed from './components/LocationFeed';



let feed = null;



window.addEventListener('DOMContentLoaded', async () => {
	const domContainer = document.querySelector('#feed');

	// Initialize React app
	feed =  ReactDOM.render(
		<LocationFeed />,
		domContainer
	);

	// Feed click listener
	document.querySelector('#feed').addEventListener('click', async (e) => {
		// Filter submit listener
		if (e.target.classList.contains('filter-submit')) {
			const fields = {
				category: e.target.parentNode.querySelector('select[name="category"]').value,
				city: e.target.parentNode.querySelector('select[name="city"]').value,
				name: e.target.parentNode.querySelector('input[name="query"]').value
			};

			feed.updateLocations(fields);
		}
	});
});
