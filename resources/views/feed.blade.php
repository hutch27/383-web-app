<x-app-layout>
	<input name="token" value="{{ env('API_SECRET') }}" type="hidden" />

	<x-slot name="header">
		<h2 class="font-semibold text-xl text-gray-800 leading-tight">
			{{ __('Feed') }}
		</h2>
	</x-slot>

	<div class="py-12">
		<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
			<div class="bg-white shadow-sm sm:rounded-lg">
				<div class="p-6 bg-white border-b border-gray-200">
					<div id="feed"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Load React. -->
	@if (env('APP_DEBUG'))
		<script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
		<script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>
	@else
		<script src="https://unpkg.com/react@17/umd/react.production.min.js" crossorigin></script>
		<script src="https://unpkg.com/react-dom@17/umd/react-dom.production.min.js" crossorigin></script>
	@endif

	<script src="/js/feed.js"></script>

</x-app-layout>
