<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p>Hello! Here you will find the current progress I have made on the back end engineer task.</p>

                    <p>The source code can be viewed <a class="text-blue-600" href="https://bitbucket.org/hutch27/383-web-app/src/master/">here</a>.</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
