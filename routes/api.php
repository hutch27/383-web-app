<?php

use App\Http\Controllers\DataUnifier;
use App\Http\Controllers\LocationController;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.token')->group(function() {
    Route::group(['prefix' => '/location'], function() {
        /*
        * Location routes
        */
        // All results
        Route::get('/all', function() {
            return response()->json(['data' => Location::all()]);
        });

        // Filter results
        Route::get('/filter', [LocationController::class, 'filter']);

        // Individual result
        // Route::get('/{locationId}', [LocationController::class, 'index']);


        /*
        * Meta routes
        */
        // Get list of all categories
        Route::get('/categories', [LocationController::class, 'listCategories']);

        // Get list of all cities
        Route::get('/cities', [LocationController::class, 'listCities']);
    });



    // Fetch external data
    Route::get('/ext/fetch', DataUnifier::class);
});
