<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('service_id')->nullable(false);
            $table->string('name')->nullable(false);
            $table->text('description');
            $table->string('latitude', 32);
            $table->string('longitude', 32);
            $table->string('address');
            $table->string('category')->default('uncategorised')->nullable(false);
            $table->string('uri')->nullable(false);
            $table->string('rating');
            $table->string('price');
            $table->string('image_uri')->nullable(false);
            $table->string('thumbnail_uri');
            $table->boolean('has_dynamic_image')->default(false);
            $table->char('uid', 64)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
