<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * Return location of Service.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->service->city;
    }

    /**
     * Return thumbanil image URI.
     *
     * @return string
     */
    public function getThumbnail()
    {
        // Return thumbnail if it exists
        if ($this->thumbnail_uri !== null)
        {
            return $this->thumbnail_uri;
        }

        // Get resized image if service provides functionality
        if ($this->has_dynamic_image)
        {
            /*
            * Could potentially make this configurable if needed
            * Also turns out that services don't provide on-the-fly image generation, just set sizes.
            */
            return str_replace('/{width}/{height}/', '/400/300/', $this->image_uri);
        }

        // Return regular image size
        if (!empty($this->image_uri)) {
            return $this->image_uri;
        }

        // Fallback to placeholder
        return '/img/location-placeholder.png';
    }
}
