<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Service;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    private int $limit = 10;



    public function filter(Request $request)
    {
        $locations = [];

        $params = [
            'category',
            'name'
        ];

        // Build query dependent on data
        $query = Location::with('service')->limit($this->limit);

        if ($request->has('page')) {
            $query->offset($request->input('page'));
        }

        if ($request->hasAny($params))
        {
            $where = [];

            foreach ($params as $param)
            {
                if ($request->has($param))
                {
                    $sanitised = str_replace('%', '\\%', $request->input($param));
                    $where[] = [$param, 'LIKE', "%{$sanitised}%"];
                }
            }

            $query->where($where);
        }

        if ($request->has('city'))
        {
            $sanitised = str_replace('%', '\\%', $request->input('city'));
            $query->whereHas('service', function($q) use ($sanitised) {
                $q->where('city', 'LIKE', $sanitised);
            });
        }

        try
        {
            $results = $query->get();
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'No results found'], 200);
        }

        foreach ($results as $res) {
            // Retrieve model from builder
            $loc = $res->getModel();

            $this->getExtraFields($loc);
            $this->sanitize($loc);

            $locations[] = $loc;
        }

        return response()->json(['data' => $locations]);
    }

    public function index(int $id)
    {
        $location = null;

        try
        {
            $location = Location::find($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Not found'], 200);
        }

        $this->getExtraFields($location);
        $this->sanitize($location);

        return response()->json(['data' => [$location]]);
    }

    public function listCategories()
    {
        $categories = [];

        try
        {
            $categories = Location::select('category')->distinct()->get()->toArray();
        } catch (\Exception $e) {
            return response()->json(['message' => 'No results found'], 200);
        }

        // Quick alphabetical sort
        sort($categories);

        // Move results into single-depth array
        $categories = array_column($categories, 'category');

        return response()->json(['data' => $categories]);
    }

    public function listCities() {
        $cities = [];

        try
        {
            $cities = Service::select('city')->distinct()->get()->toArray();
        } catch (\Exception $e) {
            return response()->json(['message' => 'No results found'], 200);
        }

        sort($cities);

        $cities = array_column($cities, 'city');

        return response()->json(['data' => $cities]);
    }

    public function range(int $page)
    {
        $locations = null;

        // Create ID array
        $idArray = range($this->limit * $page, ($this->limit + 1) * $page + $this->limit);

        try
        {
            $locations = Location::findMany($idArray)->take($this->limit);
        } catch (ModelNotFoundException $e)
        {
            return response()->json(['message' => 'No results found'], 200);
        }

        foreach ($locations as $loc) {
            $this->getExtraFields($loc);
            $this->sanitize($loc);
        }

        return response()->json(['data' => $locations->toArray()]);
    }

    /**
     * Add extra fields into data array.
     *
     * @param Location $location
     */
    private function getExtraFields(Location &$location)
    {
        // Get a thumbnail url
        $location->thumbnail = $location->getThumbnail();

        // Get city
        $location->city = $location->getCity();
    }

    /**
     * Remove any fields uneccessary for front end.
     *
     * @param Location $location
     */
    private function sanitize(Location &$location) {
        $keys = [
            'service_id',
            'thumbnail_uri',
            'has_dynamic_image',
            'uid',
            'created_at',
            'updated_at',
            'service'
        ];

        foreach ($keys as $key) {
            unset($location->$key);
        }
    }
}
