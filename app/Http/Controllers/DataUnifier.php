<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DataUnifier extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $failed = 0;
        $successful = 0;

        // Return OK repsonse on auth test
        if ($request->hasHeader('X-Test-Auth')) {
            return response()->json([], 200);
        }

        // Get list of all third-party API endpoints
        foreach (Service::all() as $service)
        {
            $data = $this->requestData($service);

            if (!isset($data['data']))
            {
                $failed++;
                continue;
            }

            $this->processData($data['data'], $service);

            $successful++;
        }

        Log::info("Data fetch complete. {$successful} successful, {$failed} failed.");

        return response()->json(['status' => 'success', 'successful' => $successful, 'failed' => $failed]);
    }

    /**
     * Convert data into standardized format and save into database.
     *
     * @param array $data
     * @param Service $service
     * @param bool $dryRun
     */
    private function processData(array $data, Service $service, bool $dryRun = false)
    {
        if (!isset($data['locations']))
        {
            return;
        }

        foreach ($data['locations'] as $location)
        {
            // Create UID for caching and to avoid duplication - a combination of location & name
            $uid = hash('sha256', $service->location . preg_replace('/[^a-z0-9]/i', '', $location['name']));

            // Skip saving data for testing calls
            if ($dryRun) {
                return;
            }

            // Check uid does not already exist (indicating location is already cached)
            if (Location::where('uid', $uid)->exists())
            {
                continue;
            }

            // Save location
            $this->store($location, $service, $uid);
        }
    }

    /**
     * Perform an HTTP request to fetch data from an external API.
     *
     * @param App\Models\Service $service
     *
     * @return array
     */
    private function requestData(Service $service)
    {
        $ch = curl_init($service->uri);
        $json = null;

        curl_setopt_array($ch, [
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5
        ]);

        // Send request to API endpoint
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        // Skip if request failed
        if (!isset($info['http_code']) || $info['http_code'] !== 200)
        {
            Log::warning("Accessing service with ID '{$service->id}' failed with response code {$info['http_code']}");
            return ['error' => "Request failed with code {$info['http_code']}"];
        }

        try
        {
            $json = json_decode($response, true, 64, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e)
        {
            Log::error("JSON parse error for response with service ID '{$service->id}'");
            return ['error' => 'JSON parse error'];
        }

        return $json;
    }

    /**
     * Store new Location in database.
     *
     * @param array $locationData
     * @param Service $service
     * @param string $uid
     */
    private function store(array $locationData, Service $service, string $uid)
    {
        $location = new Location;

        // Main data
        $location->service_id = $service->id;
        $location->name = $locationData['name'];
        $location->description = $locationData['description'] ?? null;
        $location->latitude = $locationData['latitude'] ?? null;
        $location->longitude = $locationData['longitude'] ?? null;
        $location->address = $locationData['address'] ?? null;
        $location->category = $locationData['category'] ?? 'uncategorised';
        $location->uri = $locationData['link'];
        $location->price = $locationData['price'] ?? null;

        // Store appropriate rating
        if ($service->rating_system === 10)
        {
            $location->rating = $locationData['rating'] . '/10';
        } else {
            $location->rating = $locationData['rating'] . '/5';
        }

        // Determine and store available image options
        $location->thumbnail_uri = null;

        if (array_key_exists('image', $locationData))
        {
            $location->image_uri = $locationData['image'];
        }

        if (array_key_exists('image_large', $locationData))
        {
            $location->image_uri = $locationData['image_large'];
        }

        if (array_key_exists('image_thumb', $locationData))
        {
            $location->thumbnail_uri = $locationData['image_thumb'];
        }

        if (array_key_exists('image_dynamic', $locationData))
        {
            $location->image_uri = $locationData['image_dynamic'];
            $location->has_dynamic_image = true;
        }

        // Store generated UID
        $location->uid = $uid;

        $location->save();
    }
}
